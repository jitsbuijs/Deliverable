<?php require "bootstrap.php"; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="https://use.fontawesome.com/2a318e150b.js"></script>
    <title>Deliverable</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom stylesheets -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Include Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Roboto+Slab:300,400" rel="stylesheet">

</head>

<body>

    <?php include "elements/loginForm.php"; ?>

    <nav class="navbar fixed-top navbar-expand-lg navbar-light">
        <a class="navbar-brand before" href="#">
            <img src="images/logo-light.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

            </ul>
            <button class="btn btn-outline-success deliv mr-sm-2" data-toggle="modal" data-target="#loginForm">AANMELDEN</button>
            <a href="underConstruction.html" class="btn btn-outline-success deliv my-2 my-sm-0" type="submit">MENU</a>
        </div>
    </nav>
    <div id="fb-root"></div>
    <div class="jumbotron d-flex flex-column">
        <div class="intro-container align-self-center">
            <div class="d-flex flex-column">
                <h1>
                    <span class="splash-text">Vind jouw producten</span>
                </h1>
                <h3>
                    <span class="splash-text">Daarna laten bezorgen of direct ophalen!</span>
                </h3>
            </div>
            <div class="d-flex flex-row">
                <form action="underConstruction.html" method="get" class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12 form-row">
                            <input type="text" class="col-md-9" id="text" placeholder="Zoek je product">
                            <button type="submit" class="btn btn-primary align-self-center col-md-3">ZOEKEN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="promotion-container" id="startchange">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="box-item">
                        <a href="underConstruction.html">
                            <img src="images/product/pexels-photo-276583.jpg" class="img-fill" alt="Responsive image">
                            <h4 class="caption">Design bank</h4>
                            <p class="caption"> €899,99</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-item">
                        <a href="underConstruction.html">
                            <img src="images/product/pexels-photo-346767.jpg" class="img-fill" alt="Responsive image">
                            <h4 class="caption">Polkadot rugzak</h4>
                            <p class="caption"> €59,49</p>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-item">
                        <a href="underConstruction.html">
                            <img src="images/product/pexels-photo-350417.jpg" class="img-fill" alt="Responsive image">
                            <h4 class="caption">Handgemaakte lepels</h4>
                            <p class="caption"> €13,49</p>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row pt-2 pb-5">
                <div class="col-md-12 offset-md-2 text-center">
                    <h3 class="footnote">Wat is Deliverable?</h3>
                    <p>Met Deliverable kun je het artikel vinden wat je zoekt, direct zien waar je het in jouw buurt kunt vinden
                        en vergelijken met andere aanbieders. Je kunt het dan direct zelf ophalen in de winkel, zodat je
                        het meteen in huis hebt. Lukt het je niet om het zelf op te halen, of heb je daar gewoon geen zin
                        in, dan kun je ook gebruik maken van de bezorgservice.</p>
                    <h4 class="mt-4 mb-4">Wat vind je bij Deliverble?</h4>
                    <p>Alles wat je maar kunt bedenken! Kleding, boeken, games, laptops, meubels, levensmiddelen, accessoires
                        voor in huis, sieraden, cosmetica, schoonmaakmiddelen, sportartikelen, keukengerei, gereedschap en
                        noem maar op.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8" style="padding: 0;">
                    <div class="col-md-12">
                        <div class="box-item-big">
                            <a href="underConstruction.html">
                                <img src="images/product/pexels-photo-324028.jpg" class="img-fill" alt="Responsive image">
                                <h4 class="caption">Espresso kopjes (set 4 stuks)</h4>
                                <p class="caption"> €27,50</p>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-12 hidden-md-down">
                        <div class="box-item-big">
                            <a href="underConstruction.html">
                                <img src="images/stappen.png" class="img-fill" alt="Responsive image">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <a href="underConstruction.html">
                        <img src="images/product/pexels-photo-212289.jpg" class="img-fill" alt="Responsive image">
                        <h4 class="caption-app">Download onze App voor Android en iOS</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="recruitment-container">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center pb-5">
                    <h3 class="footnote">Werk met ons samen</h3>
                    <p>Ben je ondernemer en heb je een winkel? Via Deliverable creëer je een groter bereik en komt jouw aanbod
                        terecht bij de klant die op zoek is naar juist dát product! We komen graag bij je langs om onze werkwijze
                        toe te lichten.</p>
                </div>
                <div class="col-md-4">
                    <div class="box-item">
                        <a href="underConstruction.html">
                            <img src="images/product/pexels-photo-375889.jpg" class="img-fill" alt="Responsive image">
                            <h4 class="caption">Winkeliers</h4>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-item">
                        <a href="underConstruction.html">
                            <img src="images/product/pexels-photo-541523.jpg" class="img-fill" alt="Responsive image">
                            <h4 class="caption">Vacatures</h4>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box-item">
                        <a href="underConstruction.html">
                            <img src="images/product/pexels-photo-350417.jpg" class="img-fill" alt="Responsive image">
                            <h4 class="caption">Word bezorger!</h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer>
        <div class="container hyperlinks-container">
            <div class="col-md-3 col-sm-12">
                <h5>Algemene links</h5>
                <ul>
                    <li>
                        <a href="underConstruction.html">Klantenservice</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Winkel aanmelden</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Vacatures</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Word bezorger</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Algemene voorwaarden</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Privacy statements</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Cookieverklaring</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12">
                <h5>Nieuwe winkels</h5>
                <ul>
                    <li>
                        <a href="underConstruction.html">Open32</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Omoda</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Hunkemöller</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Van Donzel</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Blokker</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Goosens</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">HoutBrox</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Jysk</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12">
                <h5>Populaire categoriën</h5>
                <ul>
                    <li>
                        <a href="underConstruction.html">Meubels</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Mode</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Fietsen</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Speelgoed</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Elektronica</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Huisdieren</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-12">
                <h5>Contact</h5>
                <p>
                    Hogeschoollaan 1
                    <br> 4818 CR Breda
                </p>
                <p>
                    Telefoon:
                    <a href="tel:+31881234567">088 123 45 67</a>
                    <br> Email: info@deliverable.dev
                </p>
                <ul>
                    <li>
                        <a href="underConstruction.html">Facebook</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Twitter</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Google+</a>
                    </li>
                    <li>
                        <a href="underConstruction.html">Youtube</a>
                    </li>
                </ul>
            </div>
        </div>


        <div class="footer-bar text-center">
            <p>&copy; Deliverable - Afbeeldingen
                <a href="https://www.pexels.com/">Pexels</a>
            </p>
        </div>
    </footer>

    </div>
    <!-- /container -->


    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>
        window.jQuery || document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"><\/script>')
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script src="js/functions.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            var scroll_start = 0;
            var startchange = $('#startchange');
            var offset = startchange.offset();
            if (startchange.length) {
                $(document).scroll(function () {
                    scroll_start = $(this).scrollTop();
                    if (scroll_start > offset.top) {
                        $(".navbar").css('background-color', '#052F5F');
                    } else {
                        $('.navbar').css('background-color', 'transparent');
                    }
                });
            }
        });
    </script>
</body>

</html>