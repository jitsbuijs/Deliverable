<?php

class Secure {

	private const CIPHER = 'AES-256-CBC';

	/**
	 * @param $value
	 * @return string
	 */
	public static function hash($value)
	{
		return hash('sha256', $value);
	}


	public static function encrypt($value, $id)
	{
		return openssl_encrypt(
			$value,
			self::CIPHER,
			self::getEncryptionKey($id),
			0,
			self::getInitializationVector()
		);
	}

	public static function decrypt($value, $id)
	{
		return openssl_decrypt(
			$value,
			self::CIPHER,
			self::getEncryptionKey($id),
			0,
			self::getInitializationVector()
		);
	}

	private static function getInitializationVector()
	{
		$randBytes = random_bytes(8);
		$randHex = bin2hex($randBytes);
		return $randHex;

		$ivlen = openssl_cipher_iv_length(self::CIPHER);
		$iv = bin2hex(openssl_random_pseudo_bytes($ivlen));
		return $iv;
	}

	/**
	 * Get encryption key
	 *
	 * @param int $id
	 * @return string
	 */
	private static function getEncryptionKey(int $id)
	{
		$credentialsFile = file_get_contents(dirname(__DIR__ ) . '/credentials.json');
		$credentials = json_decode($credentialsFile);
		return $credentials->encryptionKeys->key{$id};
	}

}