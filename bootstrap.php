<?php

/**
 * This file is used to include all required files to run the application.
 */

// CONFIG
require_once "config.php";

// UTILS
require_once "utils/secure.php";

// MODELS
require_once "models/database.php";
require_once "models/account.php";

// CONTROLLERS
require_once "controllers/login.php";