<?php require "bootstrap.php";

$account = Login::check();

if(!$account) {
	header("Location: " . HOME_URL);
} ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <script src="https://use.fontawesome.com/2a318e150b.js"></script>
    <title>Deliverable</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom stylesheets -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Include Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Roboto+Slab:300,400" rel="stylesheet">

</head>

<body class="dashboard">

    <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-dark">
        <a class="navbar-brand before" href="#">
            <img src="images/logo-light.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">

            </ul>
            <a href="api/account.php?action=logout" class="btn btn-outline-success deliv mr-sm-2">UITLOGGEN</a>
            <a href="underConstruction.html" class="btn btn-outline-success deliv my-2 my-sm-0" type="submit">MENU</a>
        </div>
    </nav>

    <!-- /container -->
    <div class="container" style="margin-top: 150px;">
        <div class="row">
            <div class="col-md-4">
                <div class="list-group" id="list-tab" role="tablist">
                    <a class="list-group-item list-group-item-action active" id="list-home-list" href="<?php echo HOME_URL; ?>/dashboard.php" role="tab" aria-controls="home">Profiel</a>
                    <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Profiel bewerken</a>
                </div>
            </div>
            <div class="col-md-8">
                <h2>Deliverable dashboard</h2>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                        <p>De onderstaande gegevens zijn opgegeven in het registratieproces van je account. Wil je de gegevens wijzigen? Klik dan op "Profiel bewerken" in het linker menu.</p>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <td>Voornaam</td>
                                    <td><?php echo $account->getFirstName(); ?></td>
                                </tr>
                                <tr>
                                    <td>Tussenvoegsel</td>
                                    <td>
                                        <?php
                                            if($account->getSurNamePrefix() == "") {
                                                echo "N.v.t";
                                            }  else {
                                                echo $account->getSurNamePrefix();
                                            }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Achternaam</td>
                                    <td><?php echo $account->getSurName(); ?></td>
                                </tr>
                                <tr>
                                    <td>Geslacht</td>
                                    <td>
                                        <?php echo Account::$genderOptions[$account->getGender()]; ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Telefoonnummer</td>
                                    <td>
                                        <?php
                                        if($account->getPhone() == "") {
                                            echo "N.v.t";
                                        }  else {
                                            echo "0" . $account->getPhone();
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Mobiel nummer</td>
                                    <td>
                                        <?php
                                        if($account->getMobile() == "") {
                                            echo "N.v.t";
                                        }  else {
                                            echo "0" . $account->getMobile();
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>E-mailadres</td>
                                    <td><?php echo $account->getEmail(); ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                        <?php include "elements/profileUpdateForm.php"; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script>
        window.jQuery || document.write('<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"><\/script>')
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <script src="js/functions.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            var scroll_start = 0;
            var startchange = $('#startchange');
            var offset = startchange.offset();
            if (startchange.length) {
                $(document).scroll(function () {
                    scroll_start = $(this).scrollTop();
                    if (scroll_start > offset.top) {
                        $(".navbar").css('background-color', '#052F5F');
                    } else {
                        $('.navbar').css('background-color', 'transparent');
                    }
                });
            }
        });
    </script>
</body>

</html>